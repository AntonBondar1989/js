
/* Создайте массив размерностью 15 элементов, выведите все элементы в обратном порядке и разделите каждый элемент спецсимволом "Облака". При загрузке страницы спросите у пользователя индекс и удалите этот элемент из массива.*/

document.write("КЛАСНАЯ РАБОТА <hr>");
var player = Array (15);
player[0] = "Никита";
player[1] = "Костя";
player[2] = "Федот";
player[3] = "Гриша";
player[4] = "Антон";
player[5] = "Миша";
player[6] = "Стас";
player[7] = "Саня";
player[8] = "Веталь";
player[9] = "Ярик";
player[10] = "Димон";
player[11] = "Леха";
player[12] = "Елисей";
player[13] = "Гвидон";
player[14] = "Илья";
player[15] = "Вовчик"; 

var number = parseInt(prompt("Введите номер игрока которого следует удалить"));

var reverseredPlayer = player.reverse();
reverseredPlayer.splice(number, -1); 

var deletedPlayer = reverseredPlayer.splice(number, 1);

var joinCloudPlayer = reverseredPlayer.join("&#9729");
document.write("Состав масива = " + joinCloudPlayer + "<br>");
document.write("Удаленный игрок = " + deletedPlayer + " <br> <hr>");


/*Создайте массив styles с элементами «Джаз» и «Блюз».
Добавьте «Рок-н-ролл» в конец.
Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
Удалите первый элемент массива и покажите его.
Вставьте «Рэп» и «Регги» в начало массива.
Повторить все то что проходили на уроке*/
document.write("ДОМАШНЯЯ РАБОТА <hr>");
var styles = ["Джаз", "Блюз"];
var long = styles.length;
document.write("Состав масива = " + styles + "<br>");
document.write("Длинна масива = " + long + "<hr>");

styles.push ("Рок-н-ролл");
long = styles.length;
document.write("Состав масива = " + styles + "<br>");
document.write("Длинна масива = " + long + "<hr>");

styles[1] = "Классика";
long = styles.length;
document.write("Состав масива = " + styles + "<br>");
document.write("Длинна масива = " + long + "<hr>");

var firstElement = styles.shift();
long = styles.length;
document.write("Состав масива = " + styles + "<br>");
document.write("Удаленный первый элемент = " + firstElement + "<br>");
document.write("Длинна масива = " + long + "<hr>");

styles.unshift("Рэп","Регги");
long = styles.length;
document.write("Состав масива = " + styles + "<br>");
document.write("Длинна масива = " + long + "<hr>");
