
let getId = id => document.getElementById(id);
//Получаем каждый размер пиццы в переменную
let smallPizza = getId("small");
let midPizza = getId("mid");
let bigPizza = getId("big");
let showPrice = getId("show_price");

//Получаем поля вводов в переменные соответсвенно категории
let clientName = getId("client_name");
let clientNumber = getId("client_number");
let clientEmail = getId("client_email");

//Кнопки сброса и подтверждения
let clearButton = getId("clear");
let acceptButton = getId("accept");

//Задаем цены для пиццы в переменные
let a = 100;
let b = 200;
let c = 300;

//Цены соус
let souce = 20;

//Цена топинг
let toping = 25;

//Переменная с ценой
let price = 0;

//Переменная с банером скидки
let discount = getId("banner");

// !!!ВЫБОР ПИЦЦЫ
//При нажатии на маленькую пиццу выводим цену
smallPizza.addEventListener("click", function () {
   showPrice.innerText = `${a}грн.`;
   price = a;
   return price;
});

//При нажатии на среднюю пиццу выводим цену
midPizza.addEventListener("click", function () {
   showPrice.innerText = `${b}грн.`;
   price = b;
   return price;
});

//При нажатии на большую пиццу выводим цену
bigPizza.addEventListener("click", function () {
   showPrice.innerText = `${c}грн.`;
   price = c;
   return price;
});


//!!!DRAG AND DROP
// !!!СОУСЫ и ТОПИНГИ
//Заносим в переменные соусы
let sauceClassic = getId('sauceClassic');
let sauceBBQ = getId('sauceBBQ');
let sauceRikotta = getId('sauceRikotta');
//Заносим топинги в переменные
let topingObichnyi = getId('moc1');
let topingFeta = getId('moc2');
let topingMozarella = getId('moc3');
let topingTelyatina = getId('telya');
let topingTomato = getId('vetch1');
let topingMashrums = getId('vetch2');

// Масив топингов и соусов
let masSouces = [sauceClassic, sauceBBQ, sauceRikotta, topingObichnyi, topingFeta, topingMozarella, topingTelyatina, topingTomato, topingMashrums];

//Какой соус
let what;

// начало операции drag
masSouces.forEach(el => el.addEventListener('dragstart', function (e) {
   this.style.border = "3px dashed red"; // меняем стиль в начале операции drag & drop
   //Визуальный эффект
   e.dataTransfer.effectAllowed = "move";
   //Какие данные переносим
   e.dataTransfer.setData("Text", this.id);
   what = el;
}, false))

// конец операции drag
masSouces.forEach(el => el.addEventListener("dragend", function (e) {
   this.style.border = ""; // удаляем стили добавленные в начале операции drag & drop 
}, false))

//Получаем пиццу в переменную
let pizza = getId('piZZa');

// перетаскиваемый объект попадает в область целевого элемента
pizza.addEventListener("dragenter", function (e) {
   this.style.border = "3px solid green";
}, false);

// перетаскиваемый элемент покидает область целевого элемента
pizza.addEventListener("dragleave", function (e) {
   this.style.border = "";
}, false);

pizza.addEventListener("dragover", function (e) {
   // отменяем стандартное обработчик события dragover.
   // Для того что бы элемент мог принять перетаскиваемые данные необходимо отменить стандартный обработчик.
   if (e.preventDefault) e.preventDefault();
   return false;
}, false);

// перетаскиваемый элемент отпущен над целевым элементом
pizza.addEventListener("drop", function (e) {
   // прекращаем дальнейшее распространение события по дереву DOM и отменяем возможный стандартный обработчик установленный браузером.
   if (e.preventDefault) e.preventDefault();
   if (e.stopPropagation) e.stopPropagation();

   this.style.border = "";


   // добавляем элемент в целевой элемент. Так как в DOM не может быть два идентичных элемента - элемент удаляется со своей старой позиции.
   // ВАРИАНТЫ СОУСОВ и ТОПИНГОВ
   if (what === sauceBBQ) {
      this.appendChild(sauceBBQ);
      price = price + souce;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "BBQ"
      p.classList = "saucesa"
      getId("show_sauces").append(p);

      p.onclick = function () {
         this.remove();
         price = price - souce;
         showPrice.innerText = `${price}грн.`;
         sauceBBQ.remove();
         getId('boxBBQ').append(sauceBBQ);
      };

   } else if (what === sauceClassic) {
      this.appendChild(sauceClassic);
      price = price + souce;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Кетчуп"
      p.classList = "saucesa"
      getId("show_sauces").append(p);

      p.onclick = function () {
         this.remove();
         price = price - souce;
         showPrice.innerText = `${price}грн.`;
         sauceClassic.remove();
         getId('boxClassic').append(sauceClassic);
      };

   } else if (what === sauceRikotta) {
      this.appendChild(sauceRikotta);
      price = price + souce;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Рікотта"
      p.classList = "saucesa"
      getId("show_sauces").append(p);

      p.onclick = function () {
         this.remove();
         price = price - souce;
         showPrice.innerText = `${price}грн.`;
         sauceRikotta.remove();
         getId('boxRiKotta').append(sauceRikotta);
      };
   } else if (what === topingObichnyi) {
      this.appendChild(topingObichnyi);
      price = price + toping;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Сир Звичайний"
      p.classList = "saucesa"
      getId("show_topings").append(p);

      p.onclick = function () {
         this.remove();
         price = price - toping;
         showPrice.innerText = `${price}грн.`;
         topingObichnyi.remove();
         getId('box_obichniy').append(topingObichnyi);
      };
   } else if (what === topingFeta) {
      this.appendChild(topingFeta);
      price = price + toping;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Сир Фета"
      p.classList = "saucesa"
      getId("show_topings").append(p);

      p.onclick = function () {
         this.remove();
         price = price - toping;
         showPrice.innerText = `${price}грн.`;
         topingFeta.remove();
         getId('box_feta').append(topingFeta);
      };
   } else if (what === topingMozarella) {
      this.appendChild(topingMozarella);
      price = price + toping;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Моцарелла"
      p.classList = "saucesa"
      getId("show_topings").append(p);

      p.onclick = function () {
         this.remove();
         price = price - toping;
         showPrice.innerText = `${price}грн.`;
         topingMozarella.remove();
         getId('box_mozarella').append(topingMozarella);
      };
   } else if (what === topingMashrums) {
      this.appendChild(topingMashrums);
      price = price + toping;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Гриби"
      p.classList = "saucesa"
      getId("show_topings").append(p);

      p.onclick = function () {
         this.remove();
         price = price - toping;
         showPrice.innerText = `${price}грн.`;
         topingMashrums.remove();
         getId('box_mashroms').append(topingMashrums);
      };
   } else if (what === topingTelyatina) {
      this.appendChild(topingTelyatina);
      price = price + toping;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Телятина"
      p.classList = "saucesa"
      getId("show_topings").append(p);

      p.onclick = function () {
         this.remove();
         price = price - toping;
         showPrice.innerText = `${price}грн.`;
         topingTelyatina.remove();
         getId('box_telya').append(topingTelyatina);
      };
   } else if (what === topingTomato) {
      this.appendChild(topingTomato);
      price = price + toping;
      showPrice.innerText = `${price}грн.`;
      let p = document.createElement('p');
      p.innerText = "Помідори"
      p.classList = "saucesa"
      getId("show_topings").append(p);

      p.onclick = function () {
         this.remove();
         price = price - toping;
         showPrice.innerText = `${price}грн.`;
         topingTomato.remove();
         getId('box_tomato').append(topingTomato);
      };
   }
   return false;
}, false);

//!!!УБЕГАЕТ СКИДКА
//При наведении на скидку - скидка убегает
discount.addEventListener("mouseover", function () {
   discount.style.bottom = `${Math.floor(Math.random() * 80)}%`;
   discount.style.right = `${Math.floor(Math.random() * 80)}%`;
})

//Масив булеанов
let mas = [];

// !!! ВАЛИДАЦИЯ ФОРМЫ И ОТПРАВКА
//Валидация имени
clientName.onblur = function () {
   if (/([A-z]){2,}/g.test(clientName.value) || /([А-я]){2,}/g.test(clientName.value)) {
      clientName.style.backgroundColor = "green";
      return mas[0] = true;
   } else {
      clientName.style.backgroundColor = "red";
      return mas[0] = false;
   }
};

//Валидация Номера
clientNumber.onblur = function () {
   if (/^\+380\d{9}$/.test(clientNumber.value)) {
      clientNumber.style.backgroundColor = "green";
      return mas[1] = true;
   } else {
      clientNumber.style.backgroundColor = "red";
      return mas[1] = false;
   }
};

//ВАЛИДАТОР ПОЧТЫ!!!!
let mailValidate = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

//Валидация почты
clientEmail.onblur = function () {
   if (mailValidate.test(clientEmail.value)) {
      clientEmail.style.backgroundColor = "green";
      return mas[2] = true;
   } else {
      clientEmail.style.backgroundColor = "red";
      return mas[2] = false;
   }
};

//Событие нажатия кнопки "Сделать заказ"
acceptButton.addEventListener("click", function () {
   if (mas[0] == true && mas[1] == true && mas[2] == true) {
      document.location = './thank-you.html';
   }
}
);

//Кнопка очистить
clearButton.addEventListener("click", function () {
   clientName.value = "";
   clientNumber.value = "";
   clientEmail.value = "";
   clientName.style.backgroundColor = "white";
   clientNumber.style.backgroundColor = "white";
   clientEmail.style.backgroundColor = "white";

})



