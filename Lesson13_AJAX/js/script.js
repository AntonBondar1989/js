//СТАРЫЙ AJAX (Класная работа)
/*let url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

//AJAX (старый)
let req = new XMLHttpRequest();

//Получаем данные
req.open("GET", url);

req.addEventListener("readystatechange", () => {
   if (req.readyState === 4 && req.status >= 200 && req.status < 300) {
      showCurrency(JSON.parse(req.responseText)); // Так передаем масив обьектов с if в функцию!
   }
   
});

req.send();

//Функция отображения курса на странице
function showCurrency (arr) {
   arr.forEach(element => {
      const {txt,rate} = element;
      console.log(txt)
      //Создаем строку и колонки куда будем запихивать курс
      const tr = document.createElement("tr"); 
      const txtTd = document.createElement("td");
      const rateTd = document.createElement("td");
      //Запихиваем курс
      txtTd.innerText = txt;
      rateTd.innerText = rate;
      tr.append(txtTd, rateTd);
      //Запихиваем в таблицу на странице
      document.querySelector("tbody").append(tr);

   });
};
*/

//НОВЫЙ AJAX
/*Вывести каждого героя отдельной карточкой с указанием: 
- Имени 
- половой принадлежности 
- рост
- цвет кожи  
- год рождения -
- планету на которой родился
  Создайте кнопку сохранить на каждой карточке. При нажатии кнопки записшите информацию в браузере*/


//Получаем адрес
let url = "https://swapi.dev/api/people";

//Делаем запрос на сервер с помощью fetch
const data = fetch(url, { method: "get" });// Получили Promise второй аргумент не обязателен(method);

//Обрабатываем Promise (работа с запросом) //then открывает Promise
let data1 = data.then((res) => res.json(), (error) => console.log(error));

//Получаем масив обьектов
data1.then((res1) => {
   res1.results.forEach(element => {
      const {name, height, birth_year, eye_color, gender, hair_color, mass, skin_color} = element;
      //Создаем карточки
      const card = document.createElement("div");
      const nameHero = document.createElement("div");
      nameHero.classList = "name";
      const heightHero = document.createElement("div");
      heightHero.classList = "all";
      const birthHero = document.createElement("div");
      birthHero.classList = "all";
      const eyeHero = document.createElement("div");
      eyeHero.classList = "all";
      const genderHero = document.createElement("div");
      genderHero.classList = "all";
      const hairHero = document.createElement("div");
      hairHero.classList = "all";
      const massHero = document.createElement("div");
      massHero.classList = "all";
      const skinHero = document.createElement("div");
      skinHero.classList = "all";
      const saveButton = document.createElement("button");
      saveButton.innerText = "Сохранить";
      //Записываем инфо в соответсвующие поля
      nameHero.innerText = `Имя: ${name}`;
      heightHero.innerText = `Рост: ${height}`;
      birthHero.innerText = `Дата Рождения: ${birth_year}`;
      eyeHero.innerText = `Цвет глаз: ${eye_color}`;
      genderHero.innerText = `Пол: ${gender}`;
      hairHero.innerText = `Цвет волос: ${hair_color}`;
      massHero.innerText = `Вес: ${mass}`;
      skinHero.innerText = `Цвет кожи: ${skin_color}`;
      //Вставляем карточки
      card.append(nameHero, heightHero, birthHero, eyeHero, genderHero, hairHero, massHero, skinHero, saveButton);
      document.querySelector("main").append(card);

      //Создаем обьект героя с свойствами
      let Hero = {
         name: name,
         height: height,
         birth_year: birth_year,
         eye_color: eye_color, 
         gender: gender, 
         hair_color: hair_color, 
         mass: mass, 
         skin_color: skin_color
      }
      
      //ПО нажатию на кнопку сохраняем данные в LocalStorage
      saveButton.addEventListener("click", function(){
          localStorage.setItem("Hero",JSON.stringify(Hero))

          saveButton.innerText = "ГОТОВО";
          saveButton.style.backgroundColor = "green"
         
      })
   });

});





