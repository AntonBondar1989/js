
//Создаем кнопку в JS
const input = document.querySelector("input");
//Запускаем процесс
input.onclick = function () {
   let size = prompt("Введите диаметр круга. Введите целое число.","10") //Запрашиваем диаметр

   input.style.display = "none"; // Убираем кнопку с экрана

   for (let i = 0; i < 100; i++) {  //Запускаем цикл создания кружков
      const body = document.querySelector("body");
      const div = document.createElement("div");
      //Задаем стили
      div.classList.add("circle");
      div.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
      div.style.width = size + "px";
      div.style.height = size + "px";
      div.style.borderRadius = "55px";
      div.style.cursor = "pointer";
      div.style.margin = "10px"
      div.style.boxShadow = `0px 0px 10px 7px hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;

      //Добавляем круг в body
      let theFirstChild = body.firstChild; // Для того что бы круги нарисовались перед скриптом в HTML
      body.insertBefore(div, theFirstChild);
      //Удаляем по клику
      div.onclick = function () {
         div.remove();
      }

   }

}




