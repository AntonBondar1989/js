

const getId = id => document.getElementById(id);
//ЗАДАЧА №1 
//Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища;

function User(name, surname) {
   this.name = name;
   this.surname = surname;
   this.show = function () {
      getId("name_surname").textContent = `Name = ${this.name};  Surname = ${this.surname};`;
   }
}
const create = new User("Ivan", "Pupkin");
create.show();

//ЗАДАЧА №2
//Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний;


getId('first_li').classList = "blue";
getId('third_li').classList = "red";

//ЗАДАЧА №3
//Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки;

let onMause = getId("onmause");

const mouse = (e) => {
   onMause.innerText = `X:${e.clientX} / Y:${e.clientY}`;
}

onMause.addEventListener("mouseover", mouse);

onMause.onmouseout = function () {
   onMause.innerText = "КООРДИНАТЫ";
}
//ЗАДАЧА №4
//Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута;

let buttonOne = getId('oneb');
let buttonTwo = getId('twob');
let buttonThree = getId('threeb');
let buttonFour = getId('fourb');

buttonOne.addEventListener("click", function (e) {
   buttonOne.innerText = "Нажата первая кнопка";
   buttonTwo.innerText = "2";
   buttonThree.innerText = "3";
   buttonFour.innerText = "4";
})
buttonTwo.addEventListener("click", function (e) {
   buttonTwo.innerText = "Нажата вторая кнопка";
   buttonOne.innerText = "1";
   buttonThree.innerText = "3";
   buttonFour.innerText = "4";
})
buttonThree.addEventListener("click", function (e) {
   buttonThree.innerText = "Нажата третья кнопка";
   buttonOne.innerText = "1";
   buttonTwo.innerText = "2";
   buttonFour.innerText = "4";
})
buttonFour.addEventListener("click", function (e) {
   buttonFour.innerText = "Нажата четвертая кнопка";
   buttonOne.innerText = "1";
   buttonTwo.innerText = "2";
   buttonThree.innerText = "3";
})


//ЗАДАЧА №5
//Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці;
let moveOn = getId("move_on");
let zone = getId("zone");

const move = function (){
   moveOn.style.top = `${Math.floor(Math.random() * 350)}px`;
   moveOn.style.left = `${Math.floor(Math.random() * 435)}px`;
   moveOn.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
   zone.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
   
}

moveOn.addEventListener("mouseover", move);

//ЗАДАЧА №6
//Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body;

let inputColor = getId("input_color");
let body = document.querySelector("body");
let buttonColor = getId("button_color");

 buttonColor.addEventListener("click", function () {
   body.style.backgroundColor = inputColor.value;

})

//ЗАДАЧА №7
//Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль;

const inputLogin = getId('login');
const buttonClear = getId('clearlogin');

inputLogin.addEventListener("keypress", (e) => {
  console.log(e.key);
})
buttonClear.addEventListener("click", () => inputLogin.value = "")

//ЗАДАЧА №8
//Створіть поле для введення даних у полі введення даних виведіть текст під полем

const typeText = getId('typetext');
const showText = getId('showtext');
const clearText = getId("cleartext")

typeText.addEventListener("keypress", (e) => {
  showText.value += e.key;
})

clearText.addEventListener("click", function () {
   typeText.value = "";
   showText.value = "";
} )


