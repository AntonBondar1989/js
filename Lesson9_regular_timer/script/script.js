/*
 Створіть програму секундомір.
1 Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
2 При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
3 Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

let counterMilisec = 0;
let counterSec = 1;
let counterMin = 1;
let startIntervalmilisec;
let startIntervalsec;
let startIntervalmin;
const getId = id => document.getElementById(id);

// Милисекунды
const count = () => {
   getId("milisec").innerText = "0" + counterMilisec;
   counterMilisec++;
   if (counterMilisec >= 10) {
      getId("milisec").innerText = counterMilisec;
   };
   if (counterMilisec == 99) {
      counterMilisec = 0
   };
}

// Секунды
const count2 = () => {
   getId("sec").innerText = "0" + counterSec;
   counterSec++;
   if (counterSec >= 10) {
      getId("sec").innerText = counterSec;
   };

   if (counterSec == 59) {
      counterSec = 0
   };
}

// Минуты
const count3 = () => {
   getId("min").innerText = "0" + counterMin;
   counterMin++;
   if (counterMin >= 10) {
      getId("min").innerText = counterMin;
   };
   if (counterMin == 59) {
      counterMin = 0
   };
}

//Кнопка Старт (запуск)




getId("Start").onclick = () => {
   startIntervalmilisec = setInterval(count, 10);
   startIntervalsec = setInterval(count2, 1000);
   startIntervalmin = setInterval(count3, 60000);
   getId("display").classList.add("green");
   getId("display").classList.remove("stopwatch-display");

}

// Кнопка СТОП
getId("Stop").onclick = () => {
   clearInterval(startIntervalmilisec);
   clearInterval(startIntervalsec);
   clearInterval(startIntervalmin);
   getId("display").classList.remove("green");
   getId("display").classList.add("red");
}

// Кнопка Сброс
getId("Reset").onclick = () => {
   clearInterval(startIntervalmilisec);
   clearInterval(startIntervalsec);
   clearInterval(startIntervalmin);
   counterMilisec = 0;
   counterSec = 1;
   counterMin = 1;
   let c1 = "00";
   let c2 = "00";
   let c3 = "00";
   getId("milisec").innerText = c1;
   getId("sec").innerText = c2;
   getId("min").innerText = c3;
   getId("display").classList.remove("red");
   getId("display").classList.add("silver");
}

/**/
/*
Реалізуйте програму перевірки телефону
1 Використовуючи JS Створіть поле для введення телефону та кнопку збереження
2 Користувач повинен ввести номер телефону у форматі 000-000-00-00
3 Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.
*/
const body = document.querySelector("body");

//Поле ввода
let input = document.createElement("input");
input.classList.add("inputZone");
input.placeholder = "000-000-00-00"
input.id = "inputId";

// Кнопка сохранить
let button = document.createElement("input");
button.classList.add("button");
button.type = "button";
button.value = "Сохранить";

// Вставляем поле ввода
let theFirstChild = body.firstChild;
body.insertBefore(input, theFirstChild);
input.before(getId("maineC"));

// Вставляем кнопку
body.insertBefore(button, theFirstChild);
button.before(getId("input"));


const legalNumberForm = /\d{3}-\d{3}-\d{2}-\d{2}/;

// Нажимаем на кнопку
button.onclick = () => {
   if (legalNumberForm.test(document.getElementById("inputId").value)) {
      button.style.backgroundColor = "green";
      document.location = ' https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
   } else {
      let div = document.createElement("div");
      div.classList.add("error1");
      div.innerText = "ОШИБКА!!!";
      body.insertBefore(div, theFirstChild);
      div.after(getId("input"));
       
   }
     
}

/*
Слайдер
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://klike.net/uploads/posts/2020-05/1588575492_4.jpg
https://klike.net/uploads/posts/2020-05/1588575506_5.jpg
https://klike.net/uploads/posts/2020-05/1588575490_7.jpg
https://klike.net/uploads/posts/2020-05/1588575570_8.jpg
https://klike.net/uploads/posts/2020-05/1588575488_10.jpg
*/


const animationBox = document.getElementById("animationBox");

const backgroundF = ["https://klike.net/uploads/posts/2020-05/1588575492_4.jpg", "https://klike.net/uploads/posts/2020-05/1588575506_5.jpg", "https://klike.net/uploads/posts/2020-05/1588575490_7.jpg", "https://klike.net/uploads/posts/2020-05/1588575570_8.jpg", "https://klike.net/uploads/posts/2020-05/1588575488_10.jpg"]
let nextBackgroundF = 0;


let go = setInterval(() => {
   animationBox.style.backgroundImage = `url(${backgroundF[nextBackgroundF++ % backgroundF.length]})`;

}, 2000)







