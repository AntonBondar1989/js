/* 
//ЗАДАЧА№1
// Создаем массив со словами
let words = [
   "программа",
   "макака",
   "прекрасный",
   "оладушек"
   ];
   // Выбираем случайное слово
   let word = words[Math.floor(Math.random() * words.length)];
   // Создаем итоговый массив
 
     const answerArray = [];
     
   for (let i = 0; i < word.length; i++) {
   answerArray[i] = "_";
   }
   let remainingLetters = word.length;
   // Игровой цикл
   while (remainingLetters > 0) {
   // Показываем состояние игры
   alert(answerArray.join(" "));
  
  //Пишем игру «Виселица» 
   // Запрашиваем вариант ответа
   let guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
   if (guess === null) {
   // Выходим из игрового цикла
   break;
   } else if (guess.length !== 1) {
   alert("Пожалуйста, введите одиночную букву.");
   } else {
   // Обновляем состояние игры
   for (let j = 0; j < word.length; j++) {
   if (word[j] === guess) {
   answerArray[j] = guess;
   remainingLetters--;
   }
   }
   }
   // Конец игрового цикла
   }
   // Отображаем ответ и поздравляем игрока
   alert(answerArray.join(" "));
   alert("Отлично! Было загадано слово " + word);// шаблонная строка!!!!!!
  */

//ЗАДАЧА №2
//Реализовать функцию для создания объекта "пользователь".Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser. При вызове функция должна спросить у вызывающего имя и фамилию. Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName. Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.



//ЗАДАЧА №3
//Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом: При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday. Создать метод getAge() который будет возвращать сколько пользователю лет. Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992). Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function CreateNewUser(firstName, lastName, birthday) {
   this.firstName = firstName;
   this.lastName = lastName;
   this.birthday = birthday
};
//Login
CreateNewUser.prototype.getLogin = function () {
   return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
};
//Age
CreateNewUser.prototype.getAge = function () {
   let age = prompt("Сколько Вам лет")
   return age
};
//Password
CreateNewUser.prototype.getPassword = function () {
   return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday
};
//Запуск конструктора и создание экземпляра
const newUser = new CreateNewUser(prompt("введите имя"), prompt("введите фамилию"), prompt("Введите дату рождения", "dd.mm.yyyy"));
//Выводим в консоль
console.log("Возраст = " + newUser.getAge());
console.log("Login = " + newUser.getLogin() + " Password = " + newUser.getPassword()); 
 

