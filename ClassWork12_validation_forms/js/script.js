
// Масив в строку (JSON.parse - это строку в масив)
localStorage.user = JSON.stringify([]);

//Создаем контсруктор который принимает все данные
class User {
   constructor(name, lastname, age, phonenumber, index) {
      this.name = name;
      this.lastname = lastname;
      this.age = age;
      this.phonenumber = phonenumber;
      this.index = index;
      this.status = true;
   }
};

//Генерируем таблицу с информацией пользователя
const creatreUserList = () => {
   let currentUser = JSON.parse(localStorage.user);//Получаем масив юзеров с памяти
   currentUser.forEach(function (element) {
      let tr = document.createElement("tr");
      let tdName = document.createElement("td");
      let tdLastName = document.createElement("td");
      let tdAge = document.createElement("td");
      let tdPhoneNumber = document.createElement("td");
      let tdIndex = document.createElement("td");
      let tdStatus = document.createElement("td");
      let inputCheckbox = document.createElement("input");

      inputCheckbox.checked = element.status;
      inputCheckbox.type = "checkbox";

      tdName.innerText = element.name;
      tdLastName.innerText = element.lastname;
      tdAge.innerText = element.age;
      tdPhoneNumber.innerText = element.phonenumber;
      tdIndex.innerText = element.index;

      tdStatus.append(inputCheckbox);
      tr.append(tdName, tdLastName, tdAge, tdPhoneNumber, tdIndex, tdStatus);
      document.querySelector("tbody").append(tr);
      
   })
}


//Создаем масив из инпутов
let [...allinputs] = document.querySelectorAll("input");

//Перебор масива с помощью MAP
let inputsRez = allinputs.map(function (element) {
   return element;
}).filter((element) => { // вернули все инпуты кроме submit
   return element.type != "button";
});

//Создаем функцию валидации
const validate = (target) => {
   switch (target.id) {
      case "name": return /^[A-z]{2,}$/i.test(target.value);
      case "lastname": return /^[А-я]{2,}$/i.test(target.value);
      case "age": return /^\d{1,2}$/i.test(target.value);
      case "phonenumber": return /^\+380\d{9}$/.test(target.value);
      case "index": return /^\d{5}$/.test(target.value);
      default: throw new Error("Неверный вызов регулярного выражения");
   }
};

//Вешаем на каждый инпут в масиве оброботчик
inputsRez.forEach((e) => {
   e.addEventListener("change", (event) => {
      console.log(validate(event.target));
   })
});

//Записываем в переменную кнопку Сохранить
let saveButton = document.querySelector("[type=button]");

//Заносим по клику сохранить в масив rez
saveButton.addEventListener("click", () => {
   //Функция валидации после нажимания кнопки
   let validateRez = inputsRez.map(function (element) { //rez - Масив с булеанов
      return validate(element);
   })

   //Проверяем нет ли ошибок и все значения true
   if (!validateRez.includes(false)) {
      //Если ошибок нет то записываем в LocalStorage
      let a = JSON.parse(localStorage.user);
      a.push(
         new User(
            ...inputsRez.map((element) => {
               return element.value
            })));
      localStorage.user = JSON.stringify(a);
      inputsRez.forEach(element => element.value = "");
      creatreUserList();
   }//Тут должен быть вывод ошибок пользователю
});


